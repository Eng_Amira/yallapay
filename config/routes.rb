Rails.application.routes.draw do

  root 'users#homepage'
  resources :countries
  resources :error_logs
  resources :audit_logs
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
