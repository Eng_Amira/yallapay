# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Country.create([{ full_name: 'Egypt', short_code: 'EG', phone_code: "+20"  }, { full_name: 'Saudia Arabia', short_code: 'SA', phone_code: "+966" }])
