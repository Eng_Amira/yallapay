# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_15_143520) do

  create_table "audit_logs", force: :cascade do |t|
    t.integer "user_id"
    t.integer "user_type"
    t.string "trace_id"
    t.string "action_type"
    t.string "action_meta"
    t.string "ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string "full_name"
    t.string "short_code"
    t.string "phone_code"
    t.boolean "status", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "error_logs", force: :cascade do |t|
    t.integer "user_id"
    t.integer "user_type"
    t.string "code"
    t.string "message"
    t.string "location"
    t.boolean "status", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "username", null: false
    t.string "email"
    t.string "encrypted_password", limit: 128
    t.integer "phone"
    t.string "account_number", null: false
    t.string "language", default: "ar", null: false
    t.integer "country_id"
    t.boolean "status", default: true, null: false
    t.boolean "verified", default: false, null: false
    t.boolean "locked", default: false, null: false
    t.integer "failed_attempts", default: 0, null: false
    t.integer "active_otp"
    t.string "otp_secret_key"
    t.string "confirmation_token", limit: 128
    t.string "remember_token", limit: 128
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email"
    t.index ["remember_token"], name: "index_users_on_remember_token"
  end

end
