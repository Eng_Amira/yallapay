class CreateCountries < ActiveRecord::Migration[5.2]
  def change
    create_table :countries do |t|
      t.string :full_name
      t.string :short_code
      t.string :phone_code
      t.boolean :status, null: false, default: true

      t.timestamps
    end
  end
end
