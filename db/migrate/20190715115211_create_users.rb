class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :username, null: false
      t.string :email
      t.string :encrypted_password, limit: 128
      t.integer :phone
      t.string :account_number, null: false
      t.string :language, null: false, default: "ar"
      t.integer :country_id
      t.boolean :status, null: false, default: true
      t.boolean :verified, null: false, default: false
      t.boolean :locked, null: false, default: false
      t.integer :failed_attempts, null: false, default: 0
      t.integer :active_otp
      t.string :otp_secret_key
      t.string :confirmation_token, limit: 128
      t.string :remember_token, limit: 128

      t.timestamps
    end

    add_index :users, :email
    add_index :users, :remember_token
  end
end
