class CreateErrorLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :error_logs do |t|
      t.integer :user_id
      t.integer :user_type
      t.string :code
      t.string :message
      t.string :location
      t.boolean :status, null: false, default: false

      t.timestamps
    end
  end
end
