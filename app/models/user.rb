class User < ApplicationRecord
  include Clearance::User
  before_create :set_user_data
  def set_user_data
    self.account_number = "YP" + rand(00000000..99999999).to_s
  end

end
