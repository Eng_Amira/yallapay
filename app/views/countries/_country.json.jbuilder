json.extract! country, :id, :full_name, :short_code, :phone_code, :status, :created_at, :updated_at
json.url country_url(country, format: :json)
