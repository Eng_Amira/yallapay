json.extract! error_log, :id, :user_id, :user_type, :code, :message, :location, :status, :created_at, :updated_at
json.url error_log_url(error_log, format: :json)
