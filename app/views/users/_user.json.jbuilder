json.extract! user, :id, :username, :phone, :account_number, :language, :country_id, :status, :verified, :locked, :login_attempts, :active_otp, :otp_secret_key, :created_at, :updated_at
json.url user_url(user, format: :json)
