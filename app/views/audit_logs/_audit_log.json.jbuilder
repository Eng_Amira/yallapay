json.extract! audit_log, :id, :user_id, :user_type, :trace_id, :action_type, :action_meta, :ip, :created_at, :updated_at
json.url audit_log_url(audit_log, format: :json)
