require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
  end

  test "should get index" do
    get users_url
    assert_response :success
  end

  test "should get new" do
    get new_user_url
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post users_url, params: { user: { account_number: @user.account_number, active_otp: @user.active_otp, country_id: @user.country_id, language: @user.language, locked: @user.locked, login_attempts: @user.login_attempts, otp_secret_key: @user.otp_secret_key, phone: @user.phone, status: @user.status, username: @user.username, verified: @user.verified } }
    end

    assert_redirected_to user_url(User.last)
  end

  test "should show user" do
    get user_url(@user)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_url(@user)
    assert_response :success
  end

  test "should update user" do
    patch user_url(@user), params: { user: { account_number: @user.account_number, active_otp: @user.active_otp, country_id: @user.country_id, language: @user.language, locked: @user.locked, login_attempts: @user.login_attempts, otp_secret_key: @user.otp_secret_key, phone: @user.phone, status: @user.status, username: @user.username, verified: @user.verified } }
    assert_redirected_to user_url(@user)
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete user_url(@user)
    end

    assert_redirected_to users_url
  end
end
